use std::ops;

const PI: f32 = 3.1415;

struct Matrix {
    vec: Vec<Vec<f32>>
}

impl Matrix {
    fn fill(h: usize, w: usize, val: f32) -> Matrix {
        Matrix {vec: vec![vec![val; w]; h]}
    }
    fn zeros(h: usize, w: usize) -> Matrix {
        Self::fill(h, w, 0.0)
    }

    fn ones(h: usize, w: usize) -> Matrix {
        Self::fill(h, w, 1.0)
    }

    fn eye(size: usize) -> Matrix {
        let mut mat = Matrix {vec: vec![vec![0.0; size]; size]};
        for i in 0..size{
            mat.vec[i][i] = 1.0;
        }
        mat
    }

    fn multiply(&self, other:  &Matrix) -> Matrix {
        let w = self.vec[0].len();
        let h2 = self.vec.len();
        let w2 = other.vec[0].len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; w2]; h2];
        for i in 0..h2{
            for j in 0..w {
                for k in 0..w2 {
                    new_vec[i][k] += self.vec[i][j] * other.vec[j][k];
                }
            }
        }
        let new_matrix: Matrix = Matrix {vec: new_vec};
        new_matrix
    }

    fn subtract(&self, other:  &Matrix) -> Matrix {
        let h = self.vec.len();
        let w = self.vec[0].len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; w]; h];
        for i in 0..h{
            for j in 0..w {
                new_vec[i][j] = self.vec[i][j] - other.vec[i][j]
            }
        }
        Matrix { vec: new_vec }
    }
    
    fn add(&self, other:  &Matrix) -> Matrix {
        let h = self.vec.len();
        let w = self.vec[0].len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; w]; h];
        for i in 0..h{
            for j in 0..w {
                new_vec[i][j] = self.vec[i][j] + other.vec[i][j]
            }
        }
        Matrix { vec: new_vec }
    }

    fn dot(&self, other:  &Matrix) -> f32 {
        let h = self.vec.len();
        let w = self.vec[0].len();
        let mut res: f32 = 0.0;
        for i in 0..h{
            for j in 0..w {
                res += self.vec[i][j] * other.vec[i][j]
            }
        }
        res
    }

    fn add_in_place(&mut self, other:  &Matrix) {
        let h = self.vec.len();
        let w = self.vec[0].len();
        for i in 0..h{
            for j in 0..w {
                self.vec[i][j] = self.vec[i][j] + other.vec[i][j];
            }
        }
    }


    fn scale(&self, scalar:  f32) -> Matrix {
        let h = self.vec.len();
        let w = self.vec[0].len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; w]; h];
        for i in 0..h{
            for j in 0..w {
                new_vec[i][j] = self.vec[i][j] * scalar;
            }
        }
        Matrix { vec: new_vec }
    }

    fn scale_in_place(&mut self, scalar:  f32) {
        let h = self.vec.len();
        let w = self.vec[0].len();
        for i in 0..h{
            for j in 0..w {
                self.vec[i][j] = self.vec[i][j] * scalar;
            }
        }
    }

    fn clone(matrix: &Matrix) -> Matrix {
        let h = matrix.vec.len();
        let w = matrix.vec[0].len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; w]; h];
        for i in 0..h{
            for j in 0..w {
                new_vec[i][j] = matrix.vec[i][j];
            }
        }
        Matrix { vec: new_vec }
    }


    fn transpose(&self) -> Matrix {
        let w = self.vec[0].len();
        let h = self.vec.len();
        let mut new_vec: Vec<Vec<f32>> = vec![vec![0.0; h]; w];
        for i in 0..h {
            for j in 0..w {
                new_vec[j][i] = self.vec[i][j];
            }
        }
        Matrix { vec: new_vec }
    }
}

impl<'a, 'b> ops::Mul<&'b Matrix> for &'a Matrix{
    type Output = Matrix;

    fn mul(self, other: &'b Matrix) -> Self::Output {
        self.multiply(other)
    }
}

trait Figure {
    fn left(&self, cords: &Matrix) -> f32;
    fn right(&self, cords: &Matrix) -> f32;
    fn compute(&self, cords: &Matrix, epsilon: f32) -> bool;
    fn angle(&self, vec: &Matrix, normal: &Matrix) -> f32;
}

struct Torus {
    r1: f32,
    r2: f32
}

impl Figure for Torus {
    fn left(&self, cords: &Matrix) -> f32 {
        let Matrix { vec } = cords;
        ((vec[0][0].powf(2.0) + vec[0][1].powf(2.0)).sqrt() - self.r1).powf(2.0) + vec[0][2].powf(2.0)
    }
    fn right(&self, _cords: &Matrix) -> f32 {
        self.r2.powf(2.0)
    }
    fn compute(&self, cords: &Matrix, epsilon: f32) -> bool {
        let left = self.left(cords);
        let right = self.right(cords);
        (left + epsilon > right)
         && (left - epsilon < right)
    }

    fn angle(&self, vec: &Matrix, normal: &Matrix) -> f32 {
        let x = vec.vec[0][0];
        let y = vec.vec[0][1];
        let z = vec.vec[0][2];
        let tan_plane = Matrix { vec: vec![
            vec![
                (1.0 - self.r1/(x*x + y*y).sqrt()) * 2.0 * x,
                (1.0 - self.r1/(x*x + y*y).sqrt()) * 2.0 * y,
                2.0 * z
            ]
        ]};
        (tan_plane.dot(&normal)/(tan_plane.dot(&tan_plane).sqrt() + normal.dot(&normal).sqrt())).acos()
    }

}

struct Camera {
    w: u16,
    h: u16,
    normal_vector: Matrix,
    horizontal_vector: Matrix,
    vertical_vector: Matrix,
    position_vector: Matrix
}

impl Camera {
    fn x_rotation_matrix(alpha: f32) -> Matrix {
        Matrix { vec:
            vec![
                vec![1.0, 0.0, 0.0],
                vec![0.0, alpha.cos(), -alpha.sin()],
                vec![0.0, alpha.sin(), alpha.cos()]
            ]
        }
    }

    fn y_rotation_matrix(alpha: f32) -> Matrix {
        Matrix { vec:
            vec![
                vec![alpha.cos(), 0.0, alpha.sin()],
                vec![0.0, 1.0, 0.0],
                vec![-alpha.sin(), 0.0, alpha.cos()]
            ]
        }
    }

    fn rotate(&self, matrix: Matrix) -> Camera {
        let normal_vector = matrix.multiply(&self.normal_vector);
        let horizontal_vector = matrix.multiply(&self.horizontal_vector);
        let vertical_vector = matrix.multiply(&self.vertical_vector);
        let position_vector = matrix.multiply(&self.position_vector);

        Camera { 
            w: self.w,
            h: self.h,
            normal_vector: normal_vector,
            horizontal_vector: horizontal_vector,
            vertical_vector: vertical_vector,
            position_vector: position_vector
         }
    }

    fn rotate_x(&self, alpha: f32) -> Camera {
        let matrix = Self::x_rotation_matrix(alpha);
        self.rotate(matrix)
    }

    fn rotate_y(&self, alpha: f32) -> Camera {
        let matrix = Self::y_rotation_matrix(alpha);
        self.rotate(matrix)
    }

    fn raycast(&self, fig: &dyn Figure, step: f32, num_steps: usize) -> Matrix {
        let normal_vector = self.normal_vector.transpose();
        let normal_reversed = normal_vector.scale(-1.0);
        let vertical_vector = self.vertical_vector.transpose();
        let horizontal_vector = self.horizontal_vector.transpose();
        let mut res = Matrix::fill(self.h.into(), self.w.into(), -1.0);
        let transposed_position = self.position_vector.transpose();

        for i in 0..=self.h {
            let position_vector = transposed_position
            .add(&horizontal_vector.scale((i as i16 - self.h as i16 / 2) as f32));
            for j in 0..=self.w {
                let mut position = position_vector
                .add(&vertical_vector.scale((j as i16 - self.w as i16 / 2) as f32));
                let iu = i as usize;
                let ju = j as usize;

                let mut line = Matrix::clone(&normal_vector);
                line.scale_in_place(step);
                let mut k = 2;
                    while k<=num_steps {
                        position.add_in_place(&line);
                        k += 1;
                        if fig.compute(&position, 2.0) {
                            let a = 1.0 - (fig.angle(&position, &normal_reversed) * 2.0 / PI);
                            res.vec[iu][ju] = a;
                            break;
                        }
                    }
            }
        }
        res
    }
}

fn print_matrix(mat: Box<Matrix>) {
    let chars = ".-,;=!ox#$@";
    for v in mat.vec.into_iter(){
        for n in v.into_iter(){
            if n > 0.0 {
                let i = (n * (chars.len() as f32 + 2.0)) as usize;
                print!("{}", chars.chars().nth(i).unwrap());
            }
            else {
                print!(" ");
            }
        }
        println!("");
    }
}

fn main() {
    let torus = Torus {r1: 8.0, r2: 5.0};
    let mut camera = Camera {
        w: 30, 
        h: 30,
        normal_vector: Matrix { vec: vec![vec![0.0], vec![0.0], vec![1.0]]},
        horizontal_vector: Matrix { vec: vec![vec![1.0], vec![0.0], vec![0.0]]},
        vertical_vector: Matrix { vec: vec![vec![0.0], vec![1.0], vec![0.0]]},
        position_vector: Matrix { vec: vec![vec![0.0], vec![0.0], vec![-15.0]]},
    };

    loop {
        let res = camera.raycast(&torus, 0.4, 50);
        print!("\x1B[2J\x1B[1;1H");
        //clearscreen::clear().expect("failed");
        print_matrix(Box::new(res));
        camera = camera.rotate_x(0.314/2.0);
        camera = camera.rotate_y(0.314/2.0);
    }
}
